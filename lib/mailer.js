const fs = require('fs');
const path = require('path');
const nodemailer = require('nodemailer');
const inlineBase64 = require('nodemailer-plugin-inline-base64');
const Inliner = require('./inliner');

const styleHandlingModes = ['asis', 'flatten', 'inline'];
const loadFile = (filepath, style) => {
  const baseDir = path.dirname(filepath);
  const patternTag = /(<img[\s\S]*?) src=['"](.+?\.(png|jpe?g|gif))['"]([\s\S]*?>)/gi;
  const patternUrl = /^https?:\/\//i;
  let pLoad;

  if (style == 'asis') {
    pLoad = Promise.resolve(fs.readFileSync(filepath, 'utf8'));
  } else {
    const inliner = new Inliner(filepath);
    pLoad = inliner[style].call(inliner);
  }

  return pLoad.then(html => {
    html = html.replace(patternTag, (match, tagStart, src, suffix, tagEnd) => {
      if (patternUrl.test(src)) {
        return match;
      }

      const data = fs.readFileSync(path.resolve(baseDir, src)).toString('base64');
      const mimeType = 'image/' + (suffix == 'jpg' ? 'jpeg' : suffix);

      return `${tagStart} src="data:${mimeType};base64,${data}"${tagEnd}`;
    });

    return html;
  });
}

class Mailer {
  constructor(config) {
    const {transport: options, defaults, style} = config;

    if (!options) {
      throw 'transport configuration is required.';
    }

    const transporter = nodemailer.createTransport(options, defaults);
    transporter.use('compile', inlineBase64());

    this.transporter = transporter;
    this.styleHandling = styleHandlingModes.indexOf(style) >= 0 ? style : 'asis';
  }

  sendFile(file, params = {}, style = undefined) {
    if (style === undefined || styleHandlingModes.indexOf(style) == -1) {
      style = this.styleHandling;
    }

    return loadFile(file, style)
      .then(content => new Promise((resolve, reject) => {
          params.html = content;

          this.transporter.sendMail(params, (err, info) => {
            if (err) {
              console.error(err.message);
              reject(err);
            } else {
              console.info('Sent.');
              resolve(info);
            }
          });
        })
      )
      .catch(err => {
        console.error(err.message);
      });
  }
}

module.exports = Mailer;
