const fs = require('fs');
const path = require('path');
const childProcess = require('child_process');

const defaults = {
  rootDir: path.dirname(__dirname),
  templateDir: 'proj_template',
};

class ProjectMaker {
  constructor(rootDir = undefined, templateDir = undefined) {
    if (rootDir === undefined) {
      rootDir = defaults.rootDir;
    }
    if (templateDir === undefined) {
      templateDir = defaults.templateDir;
    }
    this.templateDir = path.resolve(rootDir, templateDir);
  }

  create(projPath) {
    const newProjDir = path.resolve(process.cwd(), projPath);
    let cmd;

    fs.mkdirSync(newProjDir);
    if (process.platform == 'win32') {
      // omit escaping double-quotation mark b/c it's not allowed for filename on Windows
      cmd = `xcopy ${this.templateDir} "${newProjDir}" /s /e /q > NUL`;
    } else {
      cmd = `rsync -r --exclude=".*" ${this.templateDir}/* "${newProjDir.replace(/"/g, '\\"')}"`;
    }

    return new Promise((resolve, reject) => {
      childProcess.exec(cmd, (err, stdout, stderr) => {
        if (err) {
          console.error(stderr);
          reject(err);
        } else {
          if (stdout) {
            console.log(stdout);
          }
          console.log(`A new project folder has been created at: ${newProjDir}`);
          resolve();
        }
      });
    });
  }
}

module.exports = ProjectMaker;
