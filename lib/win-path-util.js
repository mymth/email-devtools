const win32 = (process.platform === 'win32');
const seps = {
  unix: {to: '/',  from: '\\', re: /\\/g},
  win:  {to: '\\', from: '/',  re: /\//g},
};

module.exports = {
  onWindows() {
    return win32;
  },

  toUnix(path) {
    return this.convert(path, 'unix');
  },

  toWin(path) {
    return this.convert(path, 'win');
  },

  convert(item, type) {
    const sep = seps[type];

    if (type != 'unix' && type != 'win') {
      type = win32 ? 'win' : 'unix';
    }
    if (typeof item == 'string') {
      if (item.indexOf(seps.from) !== -1) {
        item = item.replace(sep.re, sep.to);
      }
    } else {
      Object.keys(item).forEach(key => {
        item[key] = this.convert(item[key], type);
      });
    }

    return item;
  },
};
