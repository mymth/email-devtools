const fs = require('fs');
const path = require('path');
const archiver = require('archiver');
const minimatch = require('minimatch');

const defaults = {
  rootDir: '',    // root directory of archive
  all: false,     // whether to include dot files or not
};

const getActualPath = (path, rootDir = undefined) => {
  return rootDir ? `${rootDir}/${path}` : path;
};

const globify = (path, rootDir = undefined) => {
  const actualPath = getActualPath(path, rootDir);

  if (fs.existsSync(actualPath) && fs.statSync(actualPath).isDirectory()) {
    path += '/**';
  }

  return path;
};

class Zipper {
  constructor(options = {}) {
    this.options = Object.assign({}, defaults, options);
  }

  archive(src, dest, options) {
    if (!Array.isArray(src)) {
      src = [src];
    }

    const opts = Object.assign({}, this.options, options);
    const rootDir = path.resolve(opts.rootDir);
    const minimatchOpts = {matchBase: true};
    const srcMap = src.map(path => globify(path, rootDir));
    const destFull = path.resolve(dest);
    let errorSrcPath;

    return new Promise((resolve, reject) => {
      srcMap.find((ptn, index) => {
        if (minimatch(destFull, getActualPath(ptn, rootDir), minimatchOpts)) {
          errorSrcPath = getActualPath(src[index], opts.rootDir);
          return true;
        }
      });
      if (errorSrcPath) {
        const errMsg = `The destination cannot be the same as or included in the source: ${errorSrcPath}`;

        console.error(errMsg);
        reject(new Error(errMsg));
        return;
      }

      const archive = archiver('zip');
      const output = fs.createWriteStream(dest);
      const globOptions = {};
      let totalEntries = 0;

      if (rootDir) {
        globOptions.cwd = rootDir;
      }
      if (opts.all) {
        globOptions.dot = true;
      }

      archive
        .on('progress', progressData => {
          totalEntries = progressData.entries.total;
        })
        .on('error', err => {
          console.error(err.message)
          reject(err);
        });
      output.on('close', () => {
        if (totalEntries) {
          console.info(`${dest} has been created. (${archive.pointer()} total bytes)`);
        } else {
          const reason = src.length == 1 ? 'source does not exist' : 'none of the soueces exist';
          console.warn(`Skip archiving... (${reason})`);
          fs.unlinkSync(dest);
        }
        resolve();
      });

      archive.pipe(output);
      srcMap.forEach(srcPtn => {
        archive.glob(srcPtn, globOptions);
      });
      archive.finalize();
    });
  }
}

module.exports = Zipper;
