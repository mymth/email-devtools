const fs = require('fs');
const path = require('path');
const cheerio = require('cheerio');
const CSS = require('css');
const postcss = require('postcss');
const discardComments = require('postcss-discard-comments');
const discardEmpty = require('postcss-discard-empty');
const discardDuplicates = require('postcss-discard-duplicates');
const mqPacker = require('css-mqpacker');
const juice = require('juice');
const libtidy = require('node-libtidy');

const argErrMessage = 'The destination cannot be the same as the source';

// Since html-tidy doesn't tidy stylesheet, we combine the html & css in 3 steps:
// 1. Put a placeholder (style tag + placeholder mark) at the insertion point in the html
// 2. Tidy up the html
// 3. Replace the placeholder mark with well-formated css
const stylePlaceholder = '<style type="text/css">/*-*/</style>';
const outputHtml = ($, cssTree, tidyOptions, dest = undefined) => {
  return new Promise((resolve, reject) => {
    libtidy.tidyBuffer($.html(), tidyOptions, (exception, result) => {
      if (exception) {
        console.error(exception.message);
        reject(exception);
        return;
      }

      let html = result.output.toString();

      const css = CSS.stringify(cssTree);
      const pattern = '(<style type="text/css">(\\s*\\/\\*<!\\[CDATA\\[\\*\\/)?\\n)' +
        '((\\s*)\\/\\*-\\*\\/\\n)';
      const matches = new RegExp(pattern, 'm').exec(html);

      if (matches) {
        const indent = matches[4];
        let replace = '';

        css.split('\n').forEach(line => {
          if (line) {
            replace += `${indent}${line}\n`;
          }
        });

        html = html.replace(matches[3], replace);
      }

      if (dest) {
        fs.writeFileSync(dest, html);
        console.log(dest + ' has been created.');
      }

      resolve(html);
    });
  });
};

class Inliner {
  constructor(src) {
    this.cheerioOptions = {
      decodeEntities: false,
    },
    this.tidyOptions = {
      indent: true,
      wrap: 0,
      newline: 'LF',
      preserveEntities: true,
      tidyMark: false,
    },
    this.srcFile = path.resolve(src);
    this.html = null;
    this.css = null;
  }

  parse() {
    const commentNodeType = 8;
    const srcDir = path.dirname(this.srcFile);
    const $ = cheerio.load(fs.readFileSync(this.srcFile, 'utf8'), this.cheerioOptions);
    let styles = '';

    // remove html comments in the head
    $('head').contents().filter(function () {
      return this.nodeType == commentNodeType;
    }).remove();

    // extract all rules from the linked stylesheets and the style tags in the head
    $('link[rel="stylesheet"], style').each(function () {
      if (this.type == 'style') {
        styles += $(this).html();
      } else {
        let href = $(this).attr('href');

        if (!href.match(/^[a-z0-9]+:\/\//i)) {
          href = path.resolve(srcDir, href);
        }
        styles += fs.readFileSync(href, 'utf8');
      }
    }).remove();
    styles = styles.replace(/\s+/g, ' ');

    // split media queries and regular rules, and also filter out all other types
    return postcss([
      discardComments(),
      discardEmpty(),
      discardDuplicates(),
      mqPacker(),
    ])
      .process(styles, {from: this.sourceFile})
      .then(result => {
        this.html = $.html().replace(/\s+/g, ' ');
        this.css = result.css;
      });
  }

  flatten(dest = undefined) {
    if (dest && path.resolve(dest) == this.srcFile) {
      return Promise.reject(new Error(`${argErrMessage}: ${this.srcFile}`));
    }

    const doFlatten = () => {
      const $ = cheerio.load(this.html, this.cheerioOptions);
      const cssTree = CSS.parse(this.css);

      if (cssTree.stylesheet.rules.length) {
        $('head').append(stylePlaceholder);
      }

      return outputHtml($, cssTree, this.tidyOptions, dest);
    };

    return this.html === null ?
      this.parse().then(doFlatten) :
      Promise.resolve(doFlatten());
  }

  inline(dest = undefined) {
    if (dest && path.resolve(dest) == this.srcFile) {
      return Promise.reject(new Error(`${argErrMessage}: ${this.srcFile}`));
    }

    const inlinize = () => {
      const $ = cheerio.load(this.html, this.cheerioOptions);
      const cssTree = CSS.parse('');
      const mqTree = CSS.parse('');
      const cssRules = cssTree.stylesheet.rules;
      const mqRules = mqTree.stylesheet.rules;

      CSS.parse(this.css).stylesheet.rules.forEach(rule => {
        if (rule.type == 'rule') {
          cssRules.push(rule);
        } else if (rule.type == 'media') {
          mqRules.push(rule);
        }
      });
      juice.inlineDocument($, CSS.stringify(cssTree));
      if (mqRules.length) {
        $('head').append(stylePlaceholder);
      }

      return outputHtml($, mqTree, this.tidyOptions, dest);
    };

    return this.html === null ?
      this.parse().then(inlinize) :
      Promise.resolve(inlinize());
  }
}

module.exports = Inliner;
