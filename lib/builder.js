const Inliner = require('./inliner');
const Zipper = require('./zipper');

class Tasks {
  constructor() {
    this.queue = [];
  }

  get length() {
    return this.queue.length;
  }

  enqueue(task) {
    this.queue.push(task);
  }

  run() {
    return this.queue.reduce((p, task) => p.then(task), Promise.resolve());
  }
}

const createAction = {
  inliner(no, options, taskList) {
    const actions = [];

    if (!options || !options.src) {
      console.log(`${no}) inliner: "src" option is missing : skip...`);
      return Promise.resolve(actions);
    }

    const inliner = new Inliner(options.src);

    return inliner.parse().then(() => {
      taskList.forEach((actionDef, index) => {
        const {action: method, dest} = actionDef;
        const tNo = index + 1;

        if (typeof inliner[method] != 'function') {
          console.log(`${no}) inliner: ${tNo}) Unknown action: ${method} : skip...`);
        } else if (!dest) {
          console.log(`${no}) inliner: ${tNo}) ${method}: "dest" parameter is missing : skip...`);
        } else {
          actions.push([inliner, method, [dest]]);
        }
      });

      return actions;
    });
  },

  zipper(no, options, taskList) {
    const actions = [];
    const zipper = new Zipper(options);

    return new Promise(resolve => {
      taskList.forEach((actionDef, index) => {
        const {action: method, src, dest} = actionDef;
        const tNo = index + 1;

        if (typeof zipper[method] != 'function') {
          console.log(`${no}) zipper: ${tNo}) Unknown action: ${method} : skip...`);
        } else if (!src) {
          console.log(`${no}) zipper: ${tNo}) ${method}: "src" parameter is missing : skip...`);
        } else if (!src.length) {
          console.log(`${no}) zipper: ${tNo}) ${method}: "src" parameter is empty : skip...`);
        } else if (!dest) {
          console.log(`${no}) zipper: ${tNo}) ${method}: "dest" parameter is missing : skip...`);
        } else {
          actions.push([zipper, method, [src, dest, actionDef.options]]);
        }
      });

      resolve(actions);
    });
  },
};

const parse = (config) => {
  const tasks = new Tasks();
  const queue = [];

  config.forEach((taskDef, index) => {
    const {worker, tasks: taskList} = taskDef;
    const no = index + 1;

    if (!worker || !createAction.hasOwnProperty(worker)) {
      console.log(`${no}) Unknown worker: ${worker} : skip...`);
    } else if (!Array.isArray(taskList) || taskList.length === 0) {
      console.log(`${no}) ${worker}: No tasks : skip...`);
    } else {
      queue.push(createAction[worker].call(createAction, no, taskDef.options, taskList));
    }
  });

  return Promise.all(queue).then(results => {
    results.forEach(actions => {
      actions.forEach(action => {
        const [worker, method, args] = action;
        tasks.enqueue(() => worker[method].apply(worker, args));
      });
    });

    return tasks;
  });
};

module.exports = {
  build(config) {
    const nothingToRun = () => Promise.resolve(console.log('Nothing to run.'));

    if (!Array.isArray(config)) {
      if (typeof config == 'object') {
        config = [config];
      } else {
        return nothingToRun();
      }
    } else if (config.length === 0) {
      return nothingToRun();
    }

    return parse(config).then((tasks) => {
      return tasks.length > 0 ? tasks.run() : nothingToRun();
    });
  },
};
