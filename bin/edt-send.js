#!/usr/bin/env node
const fs = require('fs');
const path = require('path');
const minimist = require('minimist');
const Mailer = require('../lib/mailer');

const processError = err => {
  if (typeof err == 'string') {
    err = new Error(err);
  }
  console.error(err.message);

  return Promise.reject(err);
}

const sendCommand = (command, params) => {
  const usage = `
Usage: ${command} file [options]
       ${command} -h

  file            : Path to the file used for the message body.

  -f address      : Email address of the sender.(*)
  -t addresses    : Comma-separated list of recipients' email addresses.(*)
  --cc addresses  : Comma-separated list of email addresses used for CC.
  --bcc addresses : Comma-separated list of email addresses used for BCC.
  -s subject      : Subject of email.

  --style mode    : Style handling mode. [asis|flatten|inline]
                    (default: asis)
  -c config_file  : Path to config file.
                    (Default: mailer.config.js in working directory)
  -h              : Print this help.

  (*) Required when default values are not set in the config file.
`;
  let errMsg;

  if (params.h) {
    const cmdDesc = `Send the contents of a file as an email.
${usage}`;
    console.log(cmdDesc);
    return Promise.resolve();
  }

  if (params._.length === 0) {
    errMsg = `File path is required.
${usage}`;
    return processError(errMsg);
  }

  const configFile = path.resolve(params.c ? params.c : 'mailer.config.js');
  let config;

  try {
    config = require(configFile);
  } catch (err) {
    return processError(err);
  }

  const options = {};

  if (params.f) {
    options.from = params.f;
  } else if (!config.defaults || !config.defaults.from) {
    errMsg = `"From" address must be passed or specfied in the config file.
${usage}`;
    return processError(errMsg);
  }
  if (params.t) {
    options.to = params.t;
  } else if (!config.defaults || !config.defaults.to) {
    errMsg = `"To" address(es) must be passed or specfied in the config file.
${usage}`;
    return processError(errMsg);
  }
  if (params.cc) {
    options.cc = params.cc;
  }
  if (params.bcc) {
    options.bcc = params.bcc;
  }
  if (params.s) {
    options.subject = params.s;
  }

  return (new Mailer(config)).sendFile(params._[0], options, params.style);
};

if (require.main === module) {
  const argv = minimist(process.argv.slice(2), {string: ['_']});
  const cmdName = path.basename(process.argv[1], '.js');

  sendCommand(cmdName, argv).then(() => process.exit(), () => process.exit(1));
} else {
  module.exports = sendCommand;
}
