#!/usr/bin/env node
// const fs = require('fs');
const path = require('path');
const minimist = require('minimist');
const builder = require('../lib/builder');

const buildCommand = (command, params) => {
  const usage = `
Usage: ${command} [config_file]
       ${command} -h

  config_file     : Path to config file.
                    (Default: builder.config.js in working directory)
  -h              : Print this help.
`;

  if (params.h) {
    const cmdDesc = `Create html and zip files for email.
${usage}`;
    console.log(cmdDesc);
    return Promise.resolve();
  }

  const configFile = path.resolve(params._.length ? params._[0] : 'builder.config.js');
  let config;

  try {
    config = require(configFile);
  } catch (err) {
    console.error(err.message);
    return Promise.reject(err);
  }

  return Promise.resolve(builder.build(config));
};

if (require.main === module) {
  const argv = minimist(process.argv.slice(2), {string: ['_']});
  const cmdName = path.basename(process.argv[1], '.js');

  buildCommand(cmdName, argv).then(() => process.exit(), () => process.exit(1));
} else {
  module.exports = buildCommand;
}
