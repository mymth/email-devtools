#!/usr/bin/env node
const fs = require('fs');
const path = require('path');
const minimist = require('minimist');
const ProjMaker = require('../lib/proj-maker');

const createCommand = (command, params) => {
  const usage = `
Usage: ${command} [-t template] project_dir
       ${command} -h

  project_dir     : Path to the new project directory.
  -t template     : Path to a custom project template directory.
  -h              : Print this help.
`;
  let errMsg;

  if (params.h) {
    const cmdDesc = `Create a new email project directory.
${usage}`;
    console.log(cmdDesc);
    return Promise.resolve();
  }

  if (params._.length === 0) {
    errMsg = `Project directory path is required.
${usage}`;
    console.error(errMsg);
    return Promise.reject(new Error(errMsg));
  }

  let templateDir;

  if (params.t) {
    templateDir = path.resolve(params.t);
    if (!fs.existsSync(templateDir) || !fs.statSync(templateDir).isDirectory()) {
      errMsg = `${templateDir} is not a directory.`;
      console.error(errMsg);
      return Promise.reject(new Error(errMsg));
    }
  }

  return (new ProjMaker(undefined, templateDir)).create(params._[0]);
};

if (require.main === module) {
  const argv = minimist(process.argv.slice(2), {string: ['_']});
  const cmdName = path.basename(process.argv[1], '.js');

  createCommand(cmdName, argv).then(() => process.exit(), () => process.exit(1));
} else {
  module.exports = createCommand;
}
