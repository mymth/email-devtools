var mqPacker = require('css-mqpacker');
var cssnano = require('cssnano');
var perfectionist = require('perfectionist');

module.exports = {
  plugins: [
    mqPacker(),
    cssnano({minifyParams: false}),
    perfectionist({
      cascade: false,
      indentSize: 2,
      trimLeadingZero: false,
    }),
  ]
};
