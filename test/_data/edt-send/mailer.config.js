const path = require('path');
const fakeSendmail = path.resolve(__dirname, '../../fake-sendmail.js');

module.exports = {
  transport: {
    sendmail: true,
    path: fakeSendmail,
  },
};
