const path = require('path');
const fakeSendmail = path.resolve(__dirname, '../../fake-sendmail.js');

module.exports = {
  transport: {
    sendmail: true,
    path: fakeSendmail,
  },
  defaults: {
    from: 'foo@email.com',
    to: 'bar@email.com',
    cc: ['baz@email.com', 'bam@email.com'],
    bcc: ['boo@email.com', 'moo@email.com'],
    subject: 'Test mail',
  },
  style: 'inline',
};
