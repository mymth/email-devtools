module.exports = [
  {
    worker: 'inliner',
    options: {src: 'test/_proj_template/src/src.html'},
    tasks: [
      {action: 'flatten', dest: 'flatten_dest'},
      {action: 'inline', dest: 'inline_dest'},
    ],
  },
  {
    worker: 'zipper',
    options: {rootDir: 'test/_proj_template'},
    tasks: [
      {
        action: 'archive',
        src: ['zipper_src1', 'zipper_src2'],
        dest: 'zipper_dest',
        options: {rootDir: 'root'},
      },
    ],
  },
];
