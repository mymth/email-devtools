require('../bootstrap');
const fs = require('fs');
const path = require('path');
const Mailer = require('../../lib/mailer');
const config = require('../_data/mailer/config.js');

describe('emailer', function () {
  describe('constructor', function () {
    it('sets a transport object created with passed config to transport', function () {
      const mailer = new Mailer(config);

      expect(mailer.transporter.transporter.name, 'to be', 'SMTP');
      expect(mailer.transporter.transporter.options, 'to equal', config.transport);
      expect(mailer.transporter._defaults, 'to equal', config.defaults);
    });

    it('throws an error if transport property does not exist in the config', function () {
      expect(() => new Mailer({}), 'to throw', 'transport configuration is required.');
    });

    it('sets style in the config to styleHandling if it is "flatten" or "inline"', function () {
      let mailer;

      mailer = new Mailer(Object.assign({style: 'flatten'}, config));
      expect(mailer.styleHandling, 'to be', 'flatten');

      mailer = new Mailer(Object.assign({style: 'inline'}, config));
      expect(mailer.styleHandling, 'to be', 'inline');
    });
  });

  describe('sendFile', function () {
    const dataDir = `${testRoot}/_data/mailer`;
    let mailer;

    before(function () {
      mailer = new Mailer({
        transport: {streamTransport: true, buffer: true},
        defaults: config.defaults,
      });

    });

    it('sends the content of a file using passed parameters', function () {
      const testFile = path.resolve(dataDir, 'text.html');
      const params = {
        to: 'recipient@email.com',
        subject: 'test mail',
      };
      let expected = fs.readFileSync(testFile.replace('.html', '.eml'), 'utf8');

      return mailer.sendFile(testFile, params).then(info => {
        expected = expected
          .replace('<-- messageId -->', info.messageId)
          .replace('<-- date -->', new Date().toUTCString().replace('GMT', '+0000'));

        expect(info.message.toString(), 'to be', expected);
      })
    });

    it('embeds local image files to the message as inline attachments', function () {
      const testFile = path.resolve(dataDir, 'with-image.html');
      const params = {
        from: 'sender@email.com',
        to: 'foo@test.com,bar@email.com',
        cc: 'recipient@email.com',
        subject: 'test mail with images',
      };
      let expected = fs.readFileSync(testFile.replace('.html', '.eml'), 'utf8');

      return mailer.sendFile(testFile, params).then(info => {
        const message = info.message.toString();
        const pattern = 'boundary="(.+?)"' +
          '[\\s\\S]+ #1: <img src="cid:(\\w+)" ' +
          '[\\s\\S]+ #2: <img src="cid:(\\w+)" ' +
          '[\\s\\S]+ #3: <img src="cid:(\\w+)" ';

        const [, boundary, cid1, cid2, cid3] = message.match(new RegExp(pattern));

        expected = expected
          .replace('<-- messageId -->', info.messageId)
          .replace('<-- date -->', new Date().toUTCString().replace('GMT', '+0000'))
          .replace(/<-- boundary -->/g, boundary)
          .replace(/<-- cid1 -->/g, cid1)
          .replace(/<-- cid2 -->/g, cid2)
          .replace(/<-- cid3 -->/g, cid3);

        expect(message, 'to be', expected);
      });
    });

    it('does not manipulate styles if style = asis', function () {
      const testFile = path.resolve(dataDir, 'with-css.html');
      const params = {
        to: 'recipient@email.com',
        subject: 'test mail with css',
      };
      let expected = fs.readFileSync(testFile.replace('.html', '-asis.eml'), 'utf8');

      return mailer.sendFile(testFile, params, 'asis').then(info => {
        const message = info.message.toString();
        const pattern = 'boundary="(.+?)"' +
          '[\\s\\S]+<img src=3D"cid:(\\w+)" ';

        const [, boundary, cid1] = message.match(new RegExp(pattern));

        expected = expected
          .replace('<-- messageId -->', info.messageId)
          .replace('<-- date -->', new Date().toUTCString().replace('GMT', '+0000'))
          .replace(/<-- boundary -->/g, boundary)
          .replace(/<-- cid1 -->/g, cid1);

        expect(message, 'to be', expected);
      });
    });

    it('flattens styles if style = flatten', function () {
      const testFile = path.resolve(dataDir, 'with-css.html');
      const params = {
        to: 'recipient@email.com',
        subject: 'test mail with css',
      };
      let expected = fs.readFileSync(testFile.replace('.html', '-flatten.eml'), 'utf8');

      return mailer.sendFile(testFile, params, 'flatten').then(info => {
        const message = info.message.toString();
        const pattern = 'boundary="(.+?)"' +
          '[\\s\\S]+<img src=3D"cid:(\\w+)" ';

        const [, boundary, cid1] = message.match(new RegExp(pattern));

        expected = expected
          .replace('<-- messageId -->', info.messageId)
          .replace('<-- date -->', new Date().toUTCString().replace('GMT', '+0000'))
          .replace(/<-- boundary -->/g, boundary)
          .replace(/<-- cid1 -->/g, cid1);

        expect(message, 'to be', expected);
      });
    });

    it('inlines styles if style = inline', function () {
      const testFile = path.resolve(dataDir, 'with-css.html');
      const params = {
        to: 'recipient@email.com',
        subject: 'test mail with css',
      };
      let expected = fs.readFileSync(testFile.replace('.html', '-inline.eml'), 'utf8');

      return mailer.sendFile(testFile, params, 'inline').then(info => {
        const message = info.message.toString();
        const pattern = 'boundary="(.+?)"' +
          '[\\s\\S]+<img [=\\s]*src=3D"cid:(\\w+)" ';

        const [, boundary, cid1] = message.match(new RegExp(pattern));

        expected = expected
          .replace('<-- messageId -->', info.messageId)
          .replace('<-- date -->', new Date().toUTCString().replace('GMT', '+0000'))
          .replace(/<-- boundary -->/g, boundary)
          .replace(/<-- cid1 -->/g, cid1);

        expect(message, 'to be', expected);
      });
    });

    it('uses this.styleHandling if style is invalid or not passed', function () {
      const testFile = path.resolve(dataDir, 'with-css.html');
      const params = {
        to: 'recipient@email.com',
        subject: 'test mail with css',
      };
      let expected = fs.readFileSync(testFile.replace('.html', '-flatten.eml'), 'utf8');

      mailer.styleHandling = 'flatten';

      return mailer.sendFile(testFile, params, 'embed').then(info => {
        const message = info.message.toString();
        const pattern = 'boundary="(.+?)"' +
          '[\\s\\S]+<img [=\\s]*src=3D"cid:(\\w+)" ';

        const [, boundary, cid1] = message.match(new RegExp(pattern));

        expected = expected
          .replace('<-- messageId -->', info.messageId)
          .replace('<-- date -->', new Date().toUTCString().replace('GMT', '+0000'))
          .replace(/<-- boundary -->/g, boundary)
          .replace(/<-- cid1 -->/g, cid1);

        expect(message, 'to be', expected);
      }).then(() => {
        expected = fs.readFileSync(testFile.replace('.html', '-inline.eml'), 'utf8');
        mailer.styleHandling = 'inline';

        return mailer.sendFile(testFile, params).then(info => {
          const message = info.message.toString();
          const pattern = 'boundary="(.+?)"' +
            '[\\s\\S]+<img [=\\s]*src=3D"cid:(\\w+)" ';

          const [, boundary, cid1] = message.match(new RegExp(pattern));

          expected = expected
            .replace('<-- messageId -->', info.messageId)
            .replace('<-- date -->', new Date().toUTCString().replace('GMT', '+0000'))
            .replace(/<-- boundary -->/g, boundary)
            .replace(/<-- cid1 -->/g, cid1);

          expect(message, 'to be', expected);
        });
      });
    });
  });
});
