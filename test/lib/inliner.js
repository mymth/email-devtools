require('../bootstrap');
const fs = require('fs');
const path = require('path');
const cheerio = require('cheerio');
const Inliner = require('../../lib/inliner');
const {stringify, cleanOutputDir} = require('../test-utils.js');

describe('inliner', function () {
  const outDir = `${testRoot}/_output`;
  const dataDir = `${testRoot}/_data/inliner`;
  const srcHtml = `${testRoot}/_proj_template/src/src.html`;

  before(function () {
    cleanOutputDir();
  });

  describe('constructor()', function () {
    const inliner = new Inliner(srcHtml);
    const cheerioOpts = {
      decodeEntities: false,
    };
    const tidyOpts = {
      indent: true,
      wrap: 0,
      newline: 'LF',
      preserveEntities: true,
      tidyMark: false,
    };

    it(`sets ${stringify(cheerioOpts)} to cheerioOptions`, function () {
      expect(inliner.cheerioOptions, 'to equal', cheerioOpts);
    });

    it(`sets ${stringify(tidyOpts)} to tidyOptions`, function () {
      expect(inliner.tidyOptions, 'to equal', tidyOpts);
    });

    it('sets absolute path of passed src file to srcFile', function () {
      expect(inliner.srcFile, 'to be', path.resolve(srcHtml));
    });
  });

  describe('parse()', function () {
    let inliner;

    before(function () {
      inliner = new Inliner(srcHtml);

      return inliner.parse(srcHtml);
    });

    it('extracts all styles from html, compacts the html and sets it to this.html', function () {
      const expectedHtml = cheerio.load(
        fs.readFileSync(`${dataDir}/parsed-html.html`, 'utf8'),
        {decodeEntities: false}
      ).html().replace(/\s+/g, ' ');

      expect(inliner.html, 'to be', expectedHtml);
    });

    it('compacts the extracted styles and sets it to this.css', function () {
      let expectedCss = fs.readFileSync(`${dataDir}/parsed-css.css`, 'utf8').replace(/\s+/g, ' ');

      expect(inliner.css, 'to be', expectedCss);
    });
  });

  describe('flatten()', function () {
    const destHtml = `${outDir}/dest.html`;
    const expectedHtml = fs.readFileSync(`${dataDir}/flattened-html.html`, 'utf8');
    let inliner;

    before(function () {
      inliner = new Inliner(srcHtml);

      return inliner.parse();
    });

    beforeEach(function () {
      if (fs.existsSync(destHtml)) {
        fs.unlinkSync(destHtml);
      }
    })

    it('outputs the html with all the css merged into one style tag', function () {
      return inliner.flatten(destHtml).then(actualHtml => {
        expect(actualHtml, 'to be', expectedHtml);
      });
    })

    it('writes the result to file if a file path is passed', function () {
      return inliner.flatten(destHtml).then(() => {
        expect(fs.existsSync(destHtml), 'to be true');

        const actualHtml = fs.readFileSync(destHtml, 'utf8');
        expect(actualHtml, 'to be', expectedHtml);
      });
    });

    it('parses the source if it is not previousely parsed', function () {
      return new Inliner(srcHtml).flatten().then(actualHtml => {
        expect(actualHtml, 'to be', expectedHtml);
      });
    });

    it('results error if the src and the dest are the same', function () {
      const message = 'The destination cannot be the same as the source: ' + path.resolve(srcHtml);

      inliner.flatten(srcHtml).then(
        () => { throw 'expenced error did not occur' },
        err => {
          expect(err.message, 'to be', message);
        }
      );
    });
  });

  describe('inline()', function () {
    const destHtml = `${outDir}/dest.html`;
    const expectedHtml = fs.readFileSync(`${dataDir}/inlined-html.html`, 'utf8');
    let inliner;

    before(function () {
      inliner = new Inliner(srcHtml);

      return inliner.parse();
    });

    beforeEach(function () {
      if (fs.existsSync(destHtml)) {
        fs.unlinkSync(destHtml);
      }
    })

    it('outputs the html with inlined base styles and media queries merged in a style tag', function () {
      return inliner.inline().then(actualHtml => {
        expect(actualHtml, 'to be', expectedHtml);
      });
    });

    it('writes the result to a file if file path is passed', function () {
      return inliner.inline(destHtml).then(() => {
        expect(fs.existsSync(destHtml), 'to be true');

        const actualHtml = fs.readFileSync(destHtml, 'utf8');
        expect(actualHtml, 'to be', expectedHtml);
      });
    });

    it('parses the source if it is not previousely parsed', function () {
      return new Inliner(srcHtml).inline().then(actualHtml => {
        expect(actualHtml, 'to be', expectedHtml);
      });
    });

    it('results error if the src and the dest are the same', function () {
      const message = 'The destination cannot be the same as the source: ' + path.resolve(srcHtml);

      inliner.inline(srcHtml).then(
        () => { throw 'expenced error did not occur' },
        err => {
          expect(err.message, 'to be', message);
        }
      );
    });
  });
});