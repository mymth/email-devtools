require('../bootstrap');
const fs = require('fs');
const DecompressZip = require('decompress-zip');
const pathUtil = require('../../lib/win-path-util');
const Zipper = require('../../lib/zipper');
const {stringify, listDir, cleanOutputDir} = require('../test-utils.js');

describe('zipper', function () {
  const defaultOptions = {
    rootDir: '',
    all: false,
  };
  const tpltDir = `${testRoot}/_proj_template`;

  before(function () {
    cleanOutputDir();
  });

  describe('constructor()', function () {
    it(`sets the default values: ${stringify(defaultOptions)} to options`, function () {
      const zipper = new Zipper();

      expect(zipper.options, 'to equal', defaultOptions);
    });

    it(`merges the defaults + passed values if options is passed`, function () {
      const zipper = new Zipper({rootDir: 'foo/bar'});

      expect(zipper.options, 'to equal', {rootDir: 'foo/bar', all: false});
    });
  });

  describe('archive()', function () {
    const zipper = new Zipper();
    const outDir = `${testRoot}/_output`;
    const destFile = `${outDir}/test.zip`;

    const examineFileList = (zipFile, expected) => {
      return new Promise((resolve, reject) => {
        const unarchiver = new DecompressZip(zipFile);

        unarchiver.on('error', err => {
          reject(err);
        });

        unarchiver.on('list', files => {
          expect(files.slice(0).sort(), 'to equal', expected.slice(0).sort());
          resolve();
        });

        unarchiver.list();
      });
    };

    it('creates a zip file at the dest path from the src glob patterns', function () {
      const src = [`${tpltDir}/*.js`, `${tpltDir}/*/*ht*`];

      return zipper.archive(src, destFile).then(() => {
        expect(fs.existsSync(destFile), 'to be true');

        return new Promise((resolve, reject) => {
          const unarchiver = new DecompressZip(destFile);

          unarchiver.on('error', err => {
            reject(err);
          });

          unarchiver.on('extract', () => {
            const content = fs.readFileSync(`${outDir}/test/${tpltDir}/builder.config.js`, 'utf8');
            expect(content, 'to be', fs.readFileSync(`${tpltDir}/builder.config.js`, 'utf8'));

            listDir(`${outDir}/test`).then(files => {
              expect(files, 'to equal', pathUtil.convert([
                `${testRoot}/`,
                `${tpltDir}/`,
                `${tpltDir}/builder.config.js`,
                `${tpltDir}/mailer.config.js`,
                `${tpltDir}/src/`,
                `${tpltDir}/src/src.html`,
              ]));

              resolve();
            });
          });

          unarchiver.extract({path: `${outDir}/test`});
        });
      });
    });

    it('creates a zip file recursively if a directory path is given in src', function () {
      const src = [`${tpltDir}/dist`, `${tpltDir}/src`];
      const expected = [
        `${tpltDir}/dist/`,
        `${tpltDir}/src/`,
        `${tpltDir}/src/src.html`,
      ];

      return zipper.archive(src, destFile).then(() => examineFileList(destFile, expected));
    });

    it('creates a zip file with relative paths if the rootDir option is given', function () {
      const src = ['builder.config.js', 'dist', 'src'];
      const options = {rootDir: tpltDir};
      const expected = [
        'builder.config.js',
        'dist/',
        'src/',
        'src/src.html',
      ];

      return zipper.archive(src, destFile, options).then(() => examineFileList(destFile, expected));
    });

    it('includes dotfiles in zip file if the all option is true', function () {
      const src = `${tpltDir}/dist`;
      const options = {all: true};
      const expected = [
        `${tpltDir}/dist/`,
        `${tpltDir}/dist/.gitkeep`,
      ];

      return zipper.archive(src, destFile, options).then(() => examineFileList(destFile, expected));
    });

    it('uses the options property if options argument is not passed', function () {
      const zipper2 = new Zipper({rootDir: tpltDir, all: true});
      const src = ['src', 'css', 'dist'];
      const expected = [
        'src/',
        'src/src.html',
        'css/',
        'css/base.css',
        'css/email.css',
        'dist/',
        'dist/.gitkeep',
      ];

      return zipper2.archive(src, destFile).then(() => examineFileList(destFile, expected));
    });

    it('does not create zip file if passed src dees not exist', function () {
      if (fs.existsSync(destFile)) {
        fs.unlinkSync(destFile);
      }

      return zipper.archive(['foo', 'bar/baz'], destFile).then(() => {
        expect(fs.existsSync(destFile), 'to be false');
      });
    });

    it('results error if the dest matches the src', function () {
      const messageBody = 'The destination cannot be the same as or included in the source:';

      return zipper.archive(['dist', 'src'], `${tpltDir}/src/test.zip`, {rootDir: tpltDir})
        .then(
          () => {
            throw 'expected errors did not occur';
          },
          err => {
            expect(err.message, 'to be', `${messageBody} ${tpltDir}/src`);

            return zipper.archive(`${tpltDir}/builder.config.js`, `${tpltDir}/builder.config.js`)
              .then(
                () => {
                  throw 'expected errors did not occur';
                },
                err => {
                  expect(err.message, 'to be', `${messageBody} ${tpltDir}/builder.config.js`);
                }
              );
          }
         );
    });
  });
});
