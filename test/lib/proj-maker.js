require('../bootstrap');
const fs = require('fs');
const path = require('path');
const pathUtil = require('../../lib/win-path-util');
const ProjMaker = require('../../lib/proj-maker');
const {listDir, cleanOutputDir} = require('../test-utils.js');

describe('proj-maker', function () {
  describe('constructor()', function () {
    it('sets path to proj_template directory to templateDir', function () {
      const projMaker = new ProjMaker()
      expect(projMaker.templateDir, 'to be', path.resolve(rootDir, 'proj_template'));
    });

    it('sets path to given template directory to templateDir if rootDir/templateDir are passed', function () {
      const projMaker = new ProjMaker('test', '_proj_template')
      expect(projMaker.templateDir, 'to be', path.resolve('test', '_proj_template'));
    });
  });

  describe('create()', function () {
    const testTplDir = `${testRoot}/_proj_template`;
    const destination = `${testRoot}/_output/prj-dir`;
    let tplFiles;
    let projMaker;

    before(function () {
      projMaker = new ProjMaker(rootDir, testTplDir);

      return listDir(testTplDir).then(fileList => {
        tplFiles = fileList.sort();
      });
    });

    beforeEach(function () {
      cleanOutputDir();
    });

    it('copies entire templateDir to the destination', function () {
      return projMaker.create(destination).then(() => {
        expect(fs.statSync(destination).isDirectory(), 'to be true');

        return listDir(destination).then(fileList => {
          expect(fileList.sort(), 'to equal', tplFiles);
        });
      });
    });

    it('can create a project with a name including spaces', function () {
      const destination = `${testRoot}/_output/test proj dir`;

      return projMaker.create(destination).then(() => {
        expect(fs.statSync(destination).isDirectory(), 'to be true');
      });
    });

    if (!pathUtil.onWindows()) {
      it('can create a project with a name including quotation marks', function () {
        const destination = `${testRoot}/_output/proj. "test"`;

        return projMaker.create(destination).then(() => {
          expect(fs.statSync(destination).isDirectory(), 'to be true');
        });
      });
    }
  });
});
