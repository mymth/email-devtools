require('../bootstrap');
const sinon = require('sinon');
const Inliner = require('../../lib/inliner');
const Zipper = require('../../lib/zipper');
const builder = require('../../lib/builder');
const testConfig = require('../_data/builder/config.js');

describe('builder', function () {
  describe('build()', function () {
    let flattenStub;
    let inlinizeStub;
    let archiveStub;

    before(function () {
      flattenStub = sinon.stub(Inliner.prototype, 'flatten').resolves();
      inlinizeStub = sinon.stub(Inliner.prototype, 'inline').resolves();
      archiveStub = sinon.stub(Zipper.prototype, 'archive').resolves();
    });

    after(function () {
      flattenStub.restore();
      inlinizeStub.restore();
      archiveStub.restore();
    });

    afterEach(function () {
      flattenStub.reset();
      inlinizeStub.reset();
      archiveStub.reset();
    });

    it('runs tasks specified in the config file', function () {
      return builder.build(testConfig).then(() => {
        expect(flattenStub.calledOnce, 'to be true');
        expect(flattenStub.calledWith('flatten_dest'), 'to be true');
        expect(inlinizeStub.calledOnce, 'to be true');
        expect(inlinizeStub.calledWith('inline_dest'), 'to be true');
        expect(archiveStub.calledOnce, 'to be true');
        expect(archiveStub.calledWith(
          ['zipper_src1', 'zipper_src2'], 'zipper_dest', {rootDir: 'root'}
        ), 'to be true');
      });
    });

    it('skips tasks with invalid meodule/method/arguments', function () {
      var spyLog = sinon.spy(console, 'log');

      return builder.build([
        {worker: 'outliner'},
        {worker: 'inliner'},
        {worker: 'inliner', tasks: [{}]},
        {worker: 'inliner', options: {src: 'src'}, tasks: []},
        {
          worker: 'inliner', options: {src: 'test/_proj_template/src/src.html'},
          tasks: [
            {action: 'convert'},
            {action: 'flatten'},
            {action: 'inline'},
          ],
        },
        {worker: 'zipper', options: {all: true}},
        {
          worker: 'zipper',
          tasks: [
            {action: 'extract'},
            {action: 'archive'},
            {action: 'archive', src: []},
            {action: 'archive', src: ['src1']},
          ],
        },
      ]).then(() => {
        expect(spyLog.callCount, 'to be', 13);

        const logMessages = spyLog.args.map(arg => arg[0]);

        expect(logMessages[0], 'to be', '1) Unknown worker: outliner : skip...');
        expect(logMessages[12], 'to be', 'Nothing to run.');

        [
          '2) inliner: No tasks : skip...',
          '3) inliner: "src" option is missing : skip...',
          '4) inliner: No tasks : skip...',
          '5) inliner: 1) Unknown action: convert : skip...',
          '5) inliner: 2) flatten: "dest" parameter is missing : skip...',
          '5) inliner: 3) inline: "dest" parameter is missing : skip...',
          '6) zipper: No tasks : skip...',
          '7) zipper: 1) Unknown action: extract : skip...',
          '7) zipper: 2) archive: "src" parameter is missing : skip...',
          '7) zipper: 3) archive: "src" parameter is empty : skip...',
          '7) zipper: 4) archive: "dest" parameter is missing : skip...',
        ].forEach(message => {
          expect(logMessages, 'to contain', message)
        });

        spyLog.restore();
      })
    });
  });
});
