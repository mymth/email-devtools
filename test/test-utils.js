const path = require('path');
const util = require('util');
const del = require('del');
const ls = require('list-directory-contents');
const pathUtil = require('../lib/win-path-util');

const stringify = (obj) => {
  return typeof obj == 'string' ?
    obj :
    util.inspect(obj, {depth: null}).replace(/\s+/g, ' ');
};

const listDir = (target, withHidden = false) => {
  target = pathUtil.convert(target);

  return new Promise((resolve, reject) => {
    ls(target, (err, tree) => {
      if (err) {
        reject(err);
        return;
      }

      const list = [];

      process.cwd(rootDir);
      pathUtil.convert(tree).forEach((val) => {
        const relPath = val.replace(target + path.sep, '');

        if (withHidden === true || relPath.charAt(0) != '.' && relPath.indexOf(path.sep + '.') == -1) {
          list.push(relPath);
        }
      });

      resolve(list);
    });
  });
};

const cleanOutputDir = (done) => {
  const target = path.resolve(rootDir, `${testRoot}/_output`) + '/!(.gitignore)';
  del.sync(target, typeof done == 'function' ? done() : undefined);
};

module.exports = {stringify, listDir, cleanOutputDir};
