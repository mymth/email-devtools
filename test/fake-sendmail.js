#!/usr/bin/env node
const fs = require('fs');
const path = require('path');

const outputFile = path.resolve(__dirname, '_output/fake-sendmail-out.txt');
const ws = fs.createWriteStream(outputFile);

process.stdin.pipe(ws);
