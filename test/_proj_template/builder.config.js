module.exports = [
  {
    worker: 'inliner',
    options: {src: 'src/src.html'},
    tasks: [
      {action: 'inline', dest:'../_output/dest.html'},
    ],
  },
  {
    worker: 'zipper',
    options: {rootDir: 'src'},
    tasks: [
      {action: 'archive', src: ['images'], dest: '../_output/images.zip'},
    ],
  },
];
