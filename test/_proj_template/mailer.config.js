module.exports = {
  transport: {
    host: 'smtp.email.com',
    port: 465,
    secure: true,
    auth: {
      user: 'test@email.com',
      pass: '****',
    }
  },
  defaults: {
    from: 'test@email.com',
  },
};
