require('../bootstrap');
const {exec}  = require('child_process');
const fs = require('fs');
const path = require('path');
const pathUtil = require('../../lib/win-path-util');
const {listDir, cleanOutputDir} = require('../test-utils.js');

describe('edt-create', function () {
  const command = 'node bin/edt-create.js';
  const usage = `
Usage: edt-create [-t template] project_dir
       edt-create -h

  project_dir     : Path to the new project directory.
  -t template     : Path to a custom project template directory.
  -h              : Print this help.

`;

  describe('run without args', function () {
    it('outputs error message and usage', function (done) {
      const message = `Project directory path is required.
${usage}`;

      exec(command, (err, stdout, stderr) => {
        expect(err, 'to be an', 'object');
        expect(stderr, 'to be', message);

        done();
      });
    });
  });

  describe('run with -h ', function () {
    it('outputs help message', function (done) {
      const message = `Create a new email project directory.
${usage}`;

      exec(`${command} -h`, (err, stdout/*, stderr*/) => {
        expect(err, 'to be null');
        expect(stdout, 'to be', message);

        done();
      });
    });
  });

  describe('run with project_dir', function () {
    const tpltDir = `${rootDir}/proj_template`;
    let tpltFiles;

    before(function () {
      return listDir(tpltDir).then((fileList) => {
        tpltFiles = fileList;
      });
    });

    beforeEach(function () {
      cleanOutputDir();
    });

    it('creates a new project directory by copying template directory', function () {
      const destination = `${testRoot}/_output/prj-dir`;

      return new Promise((resolve) => {
        exec(`${command} ${destination}`, (err/*, stdout, stderr*/) => {
          expect(err, 'to be null');
          resolve();
        });
      }).then(() => {
        expect(fs.statSync(destination).isDirectory(), 'to be true');

        return listDir(destination).then((fileList) => {
          expect(fileList.sort(), 'to equal', tpltFiles.sort());
        });
      });
    });

    it('can create a project with a name including spaces', function (done) {
      const destination = `${testRoot}/_output/test proj dir`;
      let destPath;

      if (pathUtil.onWindows()) {
        destPath = `"${pathUtil.toWin(destination)}"`;
      } else {
        destPath = destination.replace(/ /g, '\\ ');
      }

      exec(`${command} ${destPath}`, (/*err, stdout, stderr*/) => {
        expect(fs.statSync(destination).isDirectory(), 'to be true');
        done();
      });
    });

    if (!pathUtil.onWindows()) {
      it('can create a project with a name including quotation marks', function (done) {
        const destination = `${testRoot}/_output/proj. "test"`;
        const destPath = `${testRoot}/_output/proj.\\ \\"test\\"`;

        exec(`${command} ${destPath}`, (/*err, stdout, stderr*/) => {
          expect(fs.statSync(destination).isDirectory(), 'to be true');
          done();
        });
      });
    }
  });

  describe('run with -t', function () {
    const testTplDir = `${testRoot}/_proj_template`;
    const destination = `${testRoot}/_output/prj-dir`;
    let tpltFiles;

    before(function () {
      return listDir(testTplDir).then((fileList) => {
        tpltFiles = fileList;
      });
    });

    beforeEach(function () {
      cleanOutputDir();
    });

    it('creates a new project directory by copying the specified template directory', function () {
      return new Promise((resolve) => {
        exec(`${command} -t ${testTplDir} ${destination}`, (err/*, stdout, stderr*/) => {
          expect(err, 'to be null');
          resolve();
        });
      }).then(() => {
        expect(fs.statSync(destination).isDirectory(), 'to be true');

        return listDir(destination).then((fileList) => {
          expect(fileList.sort(), 'to equal', tpltFiles.sort());
        });
      });
    });

    it('outputs error message if template_dir is not a directory', function (done) {
      exec(`${command} -t test/mocha.opt ${destination}`, (err, stdout, stderr) => {
        expect(err, 'to be an', 'object');
        expect(stderr, 'to be', path.resolve('test/mocha.opt') + ' is not a directory.\n');

        done();
      });
    });
  });
});
