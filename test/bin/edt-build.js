require('../bootstrap');
const {exec} = require('child_process');
const fs = require('fs');
const path = require('path');
const {cleanOutputDir} = require('../test-utils.js');

describe('edt-build', function () {
  const testTplDir = `${testRoot}/_proj_template`;
  const relRoot = path.relative(testTplDir, rootDir);
  const relOutDir = '../_output';
  const relDataDir = '../_data';
  const command = `node ${relRoot}/bin/edt-build.js`;
  const usage = `
Usage: edt-build [config_file]
       edt-build -h

  config_file     : Path to config file.
                    (Default: builder.config.js in working directory)
  -h              : Print this help.

`;

  before(function () {
    cleanOutputDir();
  });

  after(function () {
    if (process.cwd() != rootDir) {
      process.chdir(rootDir);
    }
  });

  describe('run with -h', function () {
    it('outputs help message', function (done) {
      const message = `Create html and zip files for email.
${usage}`;

      exec('node bin/edt-build.js -h', (err, stdout/*, stderr*/) => {
        expect(err, 'to be null');
        expect(stdout, 'to be', message);

        done();
      });
    });
  });

  describe('run without args', function () {
    it('outputs error message if ./builder.config.js does not exist', function (done) {
      const configFile = path.resolve('builder.config.js');
      const message = `Cannot find module '${configFile}'\n`;

      exec('node bin/edt-build.js', (err, stdout, stderr) => {
        expect(err, 'to be an', 'object');
        expect(stderr, 'to be', message);

        done();
      });
    });

    it('creates output file(s) using the configuration in ./builder.config.js', function (done) {
      process.chdir(path.resolve(rootDir, testTplDir));

      exec(command, (err, stdout/*, stderr*/) => {
        expect(err, 'to be null');

        const successMsg = '../_output/dest.html has been created.\n';
        const expectedHtml = fs.readFileSync(`${relDataDir}/inliner/inlined-html.html`, 'utf8');
        const actualHtml = fs.readFileSync(`${relOutDir}/dest.html`, 'utf8');

        expect(stdout, 'to be', successMsg);
        expect(actualHtml, 'to be', expectedHtml);

        done();
      });
    });
  });

  describe('run with config_file argmument', function () {
    beforeEach(function () {
      process.chdir(path.resolve(rootDir, testTplDir));
    });

    it('outputs error message if the file does not exist', function (done) {
      const configFile = path.resolve('config.js');
      const message = `Cannot find module '${configFile}'\n`;

      exec(`${command} config.js`, (err, stdout, stderr) => {
        expect(err, 'to be an', 'object');
        expect(stderr, 'to be', message);

        done();
      });
    });

    it('creates output file(s) using the configuration in the specified file', function (done) {
      exec(`${command} ${relDataDir}/edt-build/config.js` , (err, stdout/*, stderr*/) => {
        expect(err, 'to be null');

        const successMsg = '../_output/dest.html has been created.\n';
        const expectedHtml = fs.readFileSync(`${relDataDir}/inliner/flattened-html.html`, 'utf8');
        const actualHtml = fs.readFileSync(`${relOutDir}/dest.html`, 'utf8');

        expect(stdout, 'to be', successMsg);
        expect(actualHtml, 'to be', expectedHtml);

        done();
      });
    });
  });
});
