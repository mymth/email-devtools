require('../bootstrap');
const {exec}  = require('child_process');
const fs = require('fs');
const path = require('path');
const {cleanOutputDir} = require('../test-utils.js');

describe('edt-send', function () {
  const dataDir = `${rootDir}/test/_data/edt-send`;
  const relRoot = path.relative(dataDir, rootDir);
  const command = `node ${relRoot}/bin/edt-send.js`;
  const usage = `
Usage: edt-send file [options]
       edt-send -h

  file            : Path to the file used for the message body.

  -f address      : Email address of the sender.(*)
  -t addresses    : Comma-separated list of recipients' email addresses.(*)
  --cc addresses  : Comma-separated list of email addresses used for CC.
  --bcc addresses : Comma-separated list of email addresses used for BCC.
  -s subject      : Subject of email.

  --style mode    : Style handling mode. [asis|flatten|inline]
                    (default: asis)
  -c config_file  : Path to config file.
                    (Default: mailer.config.js in working directory)
  -h              : Print this help.

  (*) Required when default values are not set in the config file.

`;
  const outputFile = `${rootDir}/test/_output/fake-sendmail-out.txt`;

  describe('run without args', function () {
    it('outputs error message and usage', function (done) {
      const message = `File path is required.
${usage}`;

      exec('node bin/edt-send.js', (err, stdout, stderr) => {
        expect(err, 'to be an', 'object');
        expect(stderr, 'to be', message);

        done();
      });
    });
  });

  describe('run with -h ', function () {
    it('outputs help message', function (done) {
      const message = `Send the contents of a file as an email.
${usage}`;

      exec('node bin/edt-send.js -h', (err, stdout) => {
        expect(err, 'to be null');
        expect(stdout, 'to be', message);

        done();
      });
    });
  });

  describe('run without -f and default "from"', function () {
    before(function () {
      process.chdir(dataDir);
    });

    after(function () {
      process.chdir(rootDir);
    })

    it('outputs error message and usage', function (done) {
      const message = `"From" address must be passed or specfied in the config file.
${usage}`;

      exec(`${command} test.html -t foo@email.com`, (err, stdout, stderr) => {
        expect(err, 'to be an', 'object');
        expect(stderr, 'to be', message);

        done();
      });
    });
  });

  describe('run without -t and default "to"', function () {
    before(function () {
      process.chdir(dataDir);
    });

    after(function () {
      process.chdir(rootDir);
    })

    it('outputs error message and usage', function (done) {
      const message = `"To" address(es) must be passed or specfied in the config file.
${usage}`;

      exec(`${command} test.html -f foo@email.com`, (err, stdout, stderr) => {
        expect(err, 'to be an', 'object');
        expect(stderr, 'to be', message);

        done();
      });
    });
  });

  describe('run with -f and -t', function () {
    before(function () {
      cleanOutputDir();
      process.chdir(dataDir);
    });

    after(function () {
      process.chdir(rootDir);
    })

    it('sends email with passed "from" and "to" addresses', function (done) {
      const options = '-f foo@email.com -t bar@email.com';

      exec(`${command} test.html ${options}`, (err, stdout/*, stderr*/) => {
        expect(err, 'to be null');
        expect(stdout, 'to be', 'Sent.\n');

        const output = fs.readFileSync(outputFile, 'utf8');

        expect(output, 'to contain', 'From: foo@email.com\n');
        expect(output, 'to contain', 'To: bar@email.com\n');

        done();
      });
    });
  });

  describe('run with --cc and --bcc', function () {
    before(function () {
      cleanOutputDir();
      process.chdir(dataDir);
    });

    after(function () {
      process.chdir(rootDir);
    })

    it('sends email with passed "cc" and "bcc" addresses', function (done) {
      const cc = 'baz@email.com, bam@email.com';
      const bcc = 'boo@email.com, moo@email.com';
      const options = `-f foo@email.com -t bar@email.com, --cc "${cc}" --bcc "${bcc}"`;

      exec(`${command} test.html ${options}`, (err, stdout/*, stderr*/) => {
        expect(err, 'to be null');
        expect(stdout, 'to be', 'Sent.\n');

        const output = fs.readFileSync(outputFile, 'utf8');

        expect(output, 'to contain', `Cc: ${cc}\n`);
        expect(output, 'to contain', `Bcc: ${bcc}\n`);

        done();
      });
    });
  });

  describe('run with -s', function () {
    before(function () {
      cleanOutputDir();
      process.chdir(dataDir);
    });

    after(function () {
      process.chdir(rootDir);
    })

    it('sends email with passed subject', function (done) {
      const subject = 'Test mail';
      const options = `-f foo@email.com -t bar@email.com, -s "${subject}"`;

      exec(`${command} test.html ${options}`, (err, stdout/*, stderr*/) => {
        expect(err, 'to be null');
        expect(stdout, 'to be', 'Sent.\n');

        const output = fs.readFileSync(outputFile, 'utf8');

        expect(output, 'to contain', `Subject: ${subject}\n`);

        done();
      });
    });
  });

  describe('run with --style', function () {
    before(function () {
      cleanOutputDir();
      process.chdir(dataDir);
    });

    after(function () {
      process.chdir(rootDir);
    })

    it('sends email with passed style handling mode', function (done) {
      const options = '-f foo@email.com -t bar@email.com, --style inline';

      exec(`${command} test.html ${options}`, (err, stdout/*, stderr*/) => {
        expect(err, 'to be null');
        expect(stdout, 'to be', 'Sent.\n');

        const output = fs.readFileSync(outputFile, 'utf8');

        expect(output, 'to contain', `<p style="color: #333;">`);
        expect(output, 'not to contain', `<style type="text/css">`);

        done();
      });
    });
  });

  describe('run with -c', function () {
    before(function () {
      cleanOutputDir();
      process.chdir(dataDir);
    });

    after(function () {
      process.chdir(rootDir);
    })

    it('sends email with the settings in the passed config', function (done) {
      exec(`${command} test.html -c config.js`, (err, stdout/*, stderr*/) => {
        expect(err, 'to be null');
        expect(stdout, 'to be', 'Sent.\n');

        const output = fs.readFileSync(outputFile, 'utf8');

        expect(output, 'to contain', 'From: foo@email.com\n');
        expect(output, 'to contain', 'To: bar@email.com\n');
        expect(output, 'to contain', 'Cc: baz@email.com, bam@email.com\n');
        expect(output, 'to contain', 'Bcc: boo@email.com, moo@email.com\n');
        expect(output, 'to contain', 'Subject: Test mail\n');
        expect(output, 'to contain', `<p style="color: #333;">`);
        expect(output, 'not to contain', `<style type="text/css">`);

        done();
      });
    });

    it('overrides the settings in the config with passed args', function (done) {
      const from = 'test@email.com';
      const to = 'woo@email.com,hoo@email.com';
      const cc = 'abc@email.com';
      const bcc = 'xyz@email.com';
      const options = `-f ${from} -t ${to} --cc ${cc} --bcc ${bcc} -s zzz --style asis`;

      exec(`${command} test.html -c config.js ${options}`, (err, stdout/*, stderr*/) => {
        expect(err, 'to be null');
        expect(stdout, 'to be', 'Sent.\n');

        const output = fs.readFileSync(outputFile, 'utf8');

        expect(output, 'to contain', 'From: test@email.com\n');
        expect(output, 'to contain', 'To: woo@email.com, hoo@email.com\n');
        expect(output, 'to contain', 'Cc: abc@email.com\n');
        expect(output, 'to contain', 'Bcc: xyz@email.com\n');
        expect(output, 'to contain', 'Subject: zzz\n');
        expect(output, 'not to contain', `<p style="color: #333;">`);
        expect(output, 'to contain', `<p>`);
        expect(output, 'to contain', `<style type="text/css">`);

        done();
      });
    });
  });
});
