module.exports = {
  // See https://nodemailer.com/smtp/ for available options and the details
  // You can also use other transports Nodemailer supports (https://nodemailer.com/transports/)
  transport: {
    host: 'smtp.email.com',
    port: 465,
    secure: true,
    auth: {
      user: 'test@email.com',
      pass: '****',
    }
  },
  // See https://nodemailer.com/message/ for available options and the details
  defaults: {
    from: 'test@email.com',
  },
};
