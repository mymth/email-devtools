module.exports = [
  {
    worker: 'inliner',
    options: {src: 'src/email.src.html'},
    tasks: [
      {action: 'flatten', dest:'dist/email.html'},
      {action: 'inline', dest:'dist/email.inlined.html'},
    ],
  },
  {
    worker: 'zipper',
    options: {rootDir: 'src'},
    tasks: [
      {action: 'archive', src: ['images'], dest: 'dist/images.zip'},
    ],
  },
];
